#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Tomasz Dwojak'
SITENAME = "Tomasz Dwojak's Site"
SITEURL = 'https://tomekd.gitlab.io/tomekd.example.io'
TITLE= "bla"

PATH = 'content'
STATIC_PATHS = ['images', 'papers', 'extra']

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'en'

THEME = "./pelican-themes/Flex"

BROWSER_COLOR = '#333'
ROBOTS = 'index, follow'
SITELOGO = SITEURL + '/images/photo.jpg'
FAVICON = SITEURL + '/images/favicon.ico'

ARTICLE_EXCLUDES = STATIC_PATHS

EXTRA_PATH_METADATA = {
    'extra/custom.css': {'path': 'static/custom.css'},
    'papers/googlec0aa0662fde5f97f.html': {'path': 'googlec0aa0662fde5f97f.html'}
}

CUSTOM_CSS = 'static/custom.css'

EXTRA_TEMPLATES_PATHS = [PATH]
DIRECT_TEMPLATES = ['index', 'categories', 'authors', 'archives', 'publications']
MAIN_MENU = True


# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('CV', 'http://getpelican.com/'),
         ('Publications', 'http://python.org/'),
         ('Teaching', 'http://jinja.pocoo.org/'))

# Social widget
SOCIAL = (('linkedin', 'https://www.linkedin.com/in/tomaszdwojak'),
          ('github', 'https://github.com/tomekd'),
          ('google', 'https://scholar.google.com/citations?user=cGFnJiAAAAAJ&hl=en&oi=ao'),
          ('rss', '//blog.alexandrevicenzi.com/feeds/all.atom.xml'))

PUBLICATIONS_SRC = 'content/pubs.bib'

PLUGIN_PATHS = ['./pelican-plugins']
PLUGINS = ['pelican-bibtex']

DEFAULT_PAGINATION = False
COPYRIGHT_YEAR = 2018

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
